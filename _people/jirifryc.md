---
name: Bc. Jiří Fryč
email: frycjiri@fel.cvut.cz
phone: +420 732 374 192
type: stazista
room: T2:C2-85
position: Vedoucí IoT Specialista
photo: /img/team/fryc.jpg
---