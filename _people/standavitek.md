---
name: Ing. Stanislav Vítek, Ph.D.
email: viteks@fel.cvut.cz
phone: +420-22435-2232
type: zamestnanec
room: T2:B2-719
position: Vedoucí
photo: /img/team/vitek.png
---