---
name: Bc. Jan Zídek
email: zidekja2@fel.cvut.cz
type: stazista
room: T2:C2-85
position: IoT Specialista
photo: /img/team/zidek.jpg
---