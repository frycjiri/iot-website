---
name: LINKY
category: faculty
contact: Jiří Fryč (<a href="mailto:frycjiri@fel.cvut.cz">frycjiri@fel.cvut.cz</a>)
image: /img/projects/linky.jpg
at: Léto 2018
---
Projekt LINKY existuje už přes 2 roky. Naším cílem je převést infrastrukturu pod SVTI, vytvořit novou aplikaci pro LINKY a i novou API která bude přístupnější všem studentům.

V cíly projektu:
  * Aplikace LinkyMHD upozorňující studenty na odjezdy metra a spojů do Strahova.
  * API přístupné všem studentům, aby si mohli zkoušet různé animace na LINKY.
  * Testovací aplikace